# Dockerized Spring Boot Application

**The purpose of this repo is to test:**

- Dockerized a jar type of Spring Boot application
- Validate if a placeholder in `application.properties` can be substituted with a corresponding environment variable
- A `docker-compose.yml` used to inject the testing environment variable.


**Example command to build the image**

`docker build -t demo .`

and then use the following command to run and inject the environment variable manually

`docker run -p 8080:8080 --rm -e testing=demo_successfully demo`

**Alternatively, we can use docker compose to launch the container**

`docker-compose up --build`

Then observe if the log, see if the value is successfully injected from env

`com.example.demo.ConfigContext           : Injected 'testing' values is: intellij_injected`