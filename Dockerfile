FROM maven:3-openjdk-8 AS build-env
WORKDIR /app

COPY pom.xml ./
RUN mvn dependency:go-offline

COPY . ./
RUN mvn package

FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=./app/target/*.jar
COPY --from=build-env ${JAR_FILE} /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]