package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@Slf4j
public class ConfigContext {

    @Value("${values.testing}")
    private String testing;

    @PostConstruct
    public void postConstruction(){
        log.info("Injected 'testing' values is: {}", testing);
    }

}
